#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <chrono>

using namespace boost::archive;
using namespace std;

typedef std::chrono::duration<int64_t, std::micro> microseconds_t;

int64_t get_time_usec()
{
    static const auto t0 = chrono::high_resolution_clock::now();
    auto t = chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<microseconds_t>(t - t0);
    return duration.count();
}

int main()
{
    stringstream ss;
    double x = 2.2345, y = 3.642346247465, z = 4.7463245645724;
    double res = 0.;

    auto t1 = get_time_usec();

    for (int i = 0; i < 1000; ++ i)
    {
        ss.seekp(0);
        binary_oarchive oa(ss);
        oa << x;
        oa << y;
        oa << z;

        ss.seekg(0);
        binary_iarchive ia(ss);
        ia >> x;
        ia >> y;
        ia >> z;
        res += x;
        res += y;
        res += z;
    }

    t1 = get_time_usec() - t1;

    cout << res << " " << t1 << endl;

    return 0;
}
